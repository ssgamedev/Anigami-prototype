using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public static bool failState = true;

    GameObject currentNode;
    public GameObject finalFold;
    public GameObject thisObject;
    Vector3 currentStartPosition;



    public GameObject[] foldList;

    public int foldingPhase = 0;

    //public GameObject[] nodes;

    void Start()
    {
        EventManager.StartListening(Events.FoldingSuccess, Success);
        EventManager.StartListening(Events.FoldingFail, Failure);
        EventManager.StartListening(Events.InitiateFolding, Initiate);
        
        finalFold = GameObject.FindGameObjectWithTag("FinalFold");

        finalFold.SetActive(false);
    }

    private void Initiate()
    {
        currentNode = GameObject.Find("Node");
        currentStartPosition = currentNode.transform.position;
    }

    private void Success()
    {
        foldingPhase++;
        //NodeScript.frameToPlay = 0;
        if(foldingPhase < foldList.Length)
        {
            foldList[foldingPhase-1].SetActive(false);
            foldList[foldingPhase].SetActive(true);
        }

        if(foldingPhase == foldList.Length)
        {
            finalFold.SetActive(true);
            finalFold.transform.position = transform.position;
            Destroy(thisObject);
        }

        //Debug.Log("phase = " + foldingPhase);
    }

    private void Failure()
    {
        //foldingPhase = 0;
    }


    private void Update()
    {

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Trail") && failState == true)
        {
            foldingPhase = 0;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinwheelScript : MonoBehaviour
{
    public GameObject doorObject;
    public GameObject wheelObject;
    public Rigidbody wheelRigidBody;
    public GameObject platformObject;

    public Transform destination;
    public Vector3 freezeVector;

    Animator doorAnim;
    Animator wheelAnim;
    Animator platformAnim;

    bool following = false;



    private void Start()
    {
        doorAnim = doorObject.GetComponent<Animator>();
        //wheelAnim = wheelObject.GetComponent<Animator>();
        platformAnim = platformObject.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("WindSpot"))
        {
            doorAnim.SetTrigger("Open");
            //wheelAnim.SetTrigger("Start");
           // wheelRigidBody.useGravity = false;
        }
        
        if (other.gameObject.CompareTag("WindSpot1"))
        {
            // Debug.Log("should lerp");
            platformAnim.SetTrigger("fly");
            freezeVector = transform.position;
            //wheelObject.transform.parent = platformObject.transform;
            following = true;
            //wheelRigidBody.useGravity = false;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("WindSpot"))
        {
            doorAnim.SetTrigger("Close");
            //wheelAnim.SetTrigger("Stop");
            //wheelObject.transform.parent = null;
            following = false;
            //wheelRigidBody.useGravity = true;
        }

        if (other.gameObject.CompareTag("WindSpot1"))
        {
            //wheelAnim.SetTrigger("Stop");
            //wheelObject.transform.parent = null;
            following = false;
            //wheelRigidBody.useGravity = true;
        }

    }

    private void FixedUpdate()
    {
        if(following == true)
        {
            //wheelObject.transform.position = freezeVector;
        }
    }


    IEnumerator LerpPosition(Vector3 targetPosition, float duration)
    {
        float time = 0;
        Vector3 startPosition = platformObject.transform.position;
        while (time < duration)
        {
            platformObject.transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        platformObject.transform.position = targetPosition;
    }

}

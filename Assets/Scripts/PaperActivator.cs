using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperActivator : MonoBehaviour
{
    public GameObject blankPaperObject;
    public GameObject planeSelection;
    public GameObject pinwheelSelection;
    public GameObject planePrefab;
    public GameObject pinwheelPrefab;
    public GameObject pinwheelButton;
    public GameObject planeButton;
    public GameObject planeFinalFold;
    public GameObject pinwheelFinalFold;

    public void Menu()
    {
        planeSelection.SetActive(true);
        pinwheelSelection.SetActive(true);
        planeButton.SetActive(true);
        pinwheelButton.SetActive(true);
    }

    public void PlaneSelected()
    {
        Instantiate(planePrefab, transform.position, Quaternion.identity);
        planeFinalFold.SetActive(true);
        blankPaperObject.SetActive(false);
    }

    public void PinwheelSelected()
    {
        Instantiate(pinwheelPrefab, transform.position, Quaternion.identity);
        pinwheelFinalFold.SetActive(true);
        blankPaperObject.SetActive(false);
    }

}

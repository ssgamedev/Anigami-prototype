using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteScript : MonoBehaviour
{

    public int speed = 50;

    public Transform endMarker;

    int phase = 1;

    private void Update()
    {
        endMarker = GameObject.Find("EndPoint").transform;
        transform.position = Vector3.MoveTowards(transform.position, endMarker.position, speed * Time.deltaTime);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Perfect Trigger"))
        {
            if (Input.GetKey("space"))
            {
                phase++;
                EventManager.TriggerEvent(Events.Success);
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EndPoint"))
        {
            Destroy(gameObject);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoldingManager : MonoBehaviour
{
    public GameObject player;
    public GameObject timeline;
    public GameObject plane;
    public GameObject trigger;
    public GameObject noteManager;

    //Vector3(-7.32999992,4.90999985,-6.30999994)
    //Vector3(15.557312,53.1571541,-1.32936475e-06)

    private void Start()
    {
        EventManager.StartListening(Events.InitiateFolding, Initiate);
    }

    void Initiate()
    {
        player.SetActive(false);
        timeline.SetActive(true);
        plane.SetActive(true);
        trigger.SetActive(true);
        noteManager.SetActive(true);

        EventManager.StopListening(Events.InitiateFolding, Initiate);
        EventManager.StartListening(Events.EndFolding, End);
    }

    void End()
    {
        player.SetActive(true);
        timeline.SetActive(false);
        plane.SetActive(false);
        trigger.SetActive(false);
        noteManager.SetActive(false);

        EventManager.StopListening(Events.EndFolding, End);
        EventManager.StartListening(Events.InitiateFolding, Initiate);

    }

}

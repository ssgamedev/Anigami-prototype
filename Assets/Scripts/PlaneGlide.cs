using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneGlide : MonoBehaviour
{
    public Rigidbody rigidBody;

    // Update is called once per frame
    public void PointCoroutine()
    {
        StartCoroutine(Pointer());

    }

    IEnumerator Pointer()
    {
        rigidBody.transform.forward = Vector3.Slerp(rigidBody.transform.forward, rigidBody.velocity.normalized, Time.deltaTime);
        yield return null;
        StartCoroutine(Pointer());
    }
}

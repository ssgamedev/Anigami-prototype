using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveDistance : MonoBehaviour
{
    // set the width and height of the game object
    public float width;
    public float height;

    // set the curve function (y = f(x)) to measure distance along
    public AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    // set the starting x position of the game object
    public float xStart = 0f;

    // set the number of intervals to divide the curve into
    public int intervals = 100;


    public Vector3 startPoint;
    public Vector3 endPoint;



    private void Start()
    {
        
    }


    // calculate the distance along the curve for the game object
    public float CalculateDistanceAlongCurve(float width, float height, float xStart, GameObject targetObject)
    {
        float distanceAlong = 0f;
        float xPrevious = xStart;
        float xCurrent;
        float yCurrent;

        // get the position of the game object relative to the curve
        Vector3 positionOnCurve = GetPositionOnCurve(targetObject.transform.position);

        for (int i = 1; i <= intervals; i++)
        {
            // calculate the current x and y values on the curve
            xCurrent = xStart + (i / (float)intervals);
            yCurrent = curve.Evaluate(xCurrent);

            // calculate the distance between the previous and current points on the curve
            float dx = xCurrent - xPrevious;
            float dy = curve.Evaluate(xCurrent) - curve.Evaluate(xPrevious);
            float segmentLength = Mathf.Sqrt((dx * dx) + (dy * dy));

            // calculate the width and height of the game object relative to the curve
            float widthOnCurve = width * Mathf.Cos(Mathf.Atan2(dy, dx));
            float heightOnCurve = height * Mathf.Sin(Mathf.Atan2(dy, dx));

            // check if the game object is within the current segment of the curve
            if (positionOnCurve.x >= xPrevious && positionOnCurve.x <= xCurrent)
            {
                // calculate the distance along the curve for the game object
                float t = Mathf.InverseLerp(xPrevious, xCurrent, positionOnCurve.x);
                float heightAtT = heightOnCurve + ((height - heightOnCurve) * t);
                float distanceAtT = distanceAlong + ((segmentLength * t) * heightAtT);
                return distanceAtT;
            }

            // update the total distance along the curve
            distanceAlong += segmentLength * ((heightOnCurve + height) / 2f);

            // update the previous x value
            xPrevious = xCurrent;
        }

        // if the game object is not on the curve, return the total distance along the curve
        return distanceAlong;
    }

    // get the position of a point on the curve closest to the given position
    private Vector3 GetPositionOnCurve(Vector3 position)
    {
        float distanceSqr = float.MaxValue;
        Vector3 positionOnCurve = Vector3.zero;

        for (int i = 0; i <= intervals; i++)
        {
            float t = (float)i / intervals;
            Vector3 point = new Vector3(xStart + (t * (1f - xStart)), curve.Evaluate(xStart + (t * (1f - xStart))), 0f);
            float d = Vector3.SqrMagnitude(position - point);
            if (d < distanceSqr)
            {
                distanceSqr = d;
                positionOnCurve = point;
            }
        }

        return positionOnCurve;
    }
}

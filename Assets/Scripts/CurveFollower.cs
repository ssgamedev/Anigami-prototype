using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveFollower : MonoBehaviour
{
    public Vector3 startPoint;
    public Vector3 endPoint;
    public float height;

    public Transform targetObject;
    public float curveLength;

    private void Start()
    {
        // Calculate the total length of the curve
        curveLength = CalculateCurveLength(startPoint, endPoint, height);
    }

    private void Update()
    {
        // Calculate the distance along the curve for the target object
        float distanceAlongCurve = CalculateDistanceAlongCurve(targetObject.position, startPoint, endPoint, height);

        // Output the result to the console
        Debug.Log("Distance along curve: " + distanceAlongCurve);
    }

    private float CalculateCurveLength(Vector3 start, Vector3 end, float height)
    {
        // Calculate the length of the curve using the formula for a parabolic arc
        float a = Vector3.Distance(start, end) / 2f;
        float b = height;
        float c = Mathf.Sqrt(Mathf.Pow(a, 2) + Mathf.Pow(b, 2));
        return 2 * Mathf.PI * c;
    }

    private float CalculateDistanceAlongCurve(Vector3 position, Vector3 start, Vector3 end, float height)
    {
        // Calculate the distance along the curve using the formula for a parabolic arc
        float a = Vector3.Distance(start, end) / 2f;
        float b = height;
        float c = Mathf.Sqrt(Mathf.Pow(a, 2) + Mathf.Pow(b, 2));

        Vector3 dir = (end - start).normalized;
        Vector3 perp = Vector3.Cross(dir, Vector3.up).normalized;
        float t = Vector3.Dot(position - start, dir);
        float y = Vector3.Dot(position - start, perp);
        float x = Mathf.Sqrt(Mathf.Pow(c, 2) - Mathf.Pow(y - b, 2));

        float distanceAlongCurve = 2 * c * Mathf.Asin(x / (2 * c));
        if (t < 0)
        {
            distanceAlongCurve = -distanceAlongCurve;
        }
        else if (t > Vector3.Distance(start, end))
        {
            distanceAlongCurve += Vector3.Distance(start, end);
        }

        return distanceAlongCurve;
    }
}

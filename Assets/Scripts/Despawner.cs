using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawner : MonoBehaviour
{

    Vector3 spawnPoint;

    void Start()
    {
        spawnPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < 0.2f)
        {
            transform.position = spawnPoint;
        }
    }
}

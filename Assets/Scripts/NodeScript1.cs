//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class NodeScript : MonoBehaviour
//{
//    public bool inTrail = false;

//    public Transform startPoint;
//    public Transform endPoint;

//    public Transform[] controlPoints;
//    public float stepSize = 0.1f;
//    public float rayLength = 10f;
//    public LayerMask hitLayers;

//    public float height;

//    public Transform targetObject;
//    public float curveLength;

//    //Draw curve

//    public int resolution = 10;
//    private LineRenderer lineRenderer;

//    //line break for now

//    public Quaternion startRotation;

//    public Rigidbody nodeRigidBody;
//    GameObject nodeObject;
//    GameObject archObject;
//    GameObject endPointObject;
//    GameObject heightObject;

//    public bool failState = NodeManager.failState;

//    public float startDistance;
//    public float currentDistanceFromEnd;
//    public float totalDistance;
//    public static float frameToPlay;

//    //line break for now

//    void Start()
//    {
//        lineRenderer = gameObject.GetComponent<LineRenderer>();
//        lineRenderer.positionCount = 0;
//        lineRenderer.startWidth = 0.1f;
//        lineRenderer.endWidth = 0.1f;
//        lineRenderer.useWorldSpace = true;
//        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));

//    }

//    private void OnEnable()
//    {        
//        nodeObject = GameObject.Find("Node");
//        archObject = GameObject.Find("Arch");
//        heightObject = GameObject.Find("Height");
//        endPointObject = GameObject.Find("End Point");

//        //height = heightObject.transform.position.y - startPoint.transform.position.y;

//        endPoint = endPointObject.transform;
//        startPoint = nodeObject.transform;


//        startRotation = nodeObject.transform.rotation;
//        //curveLength = CalculateCurveLength(startPoint, endPoint, height);

//        targetObject = nodeObject.transform;

//        height = heightObject.transform.position.y - startPoint.transform.position.y;

//        nodeRigidBody.velocity = Vector3.zero;
//        //transform.position = startPoint;
//        transform.rotation = Quaternion.Euler(0,0,0);
//        //archObject.GetComponent<MeshRenderer>().enabled = true;
//        nodeObject.GetComponent<MeshRenderer>().enabled = true;
//        totalDistance = Vector3.Distance(endPoint.transform.position, startPoint.transform.position);
//    }

//    private void Update()
//    {
//        Vector3[] curvePoints = new Vector3[controlPoints.Length + 2];
//        curvePoints[0] = startPoint.position;
//        curvePoints[curvePoints.Length - 1] = endPoint.position;

//        for (int i = 0; i < controlPoints.Length; i++)
//        {
//            curvePoints[i + 1] = controlPoints[i].position;
//        }

//        float totalRaycastLength = 0f;
//        Vector3 previousPointOnCurve = Vector3.zero;
//        lineRenderer.positionCount = 0;

//        for (float t = 0; t <= 1; t += stepSize)
//        {
//            Vector3 pointOnCurve = CalculateBezierPoint(t, curvePoints);

//            if (t > 0)
//            {
//                totalRaycastLength += Vector3.Distance(previousPointOnCurve, pointOnCurve);
//            }

//            RaycastHit hit;
//            if (Physics.Raycast(pointOnCurve, Vector3.down, out hit, rayLength, hitLayers))
//            {
//                Debug.DrawRay(pointOnCurve, Vector3.down * hit.distance, Color.green);
//                lineRenderer.positionCount++;
//                lineRenderer.SetPosition(lineRenderer.positionCount - 1, pointOnCurve);
//            }
//            else
//            {
//                Debug.DrawRay(pointOnCurve, Vector3.down * rayLength, Color.red);
//            }
//            previousPointOnCurve = pointOnCurve;
//        }

//        Debug.Log("Total raycast length: " + totalRaycastLength);

//        //currentDistanceFromEnd = Vector3.Distance(nodeRigidBody.transform.position, endPoint);

//        transform.rotation = startRotation;
//        nodeRigidBody.velocity = Vector3.zero;

//        // Calculate the distance along the curve for the target object
//        //float distanceAlongCurve = CalculateDistanceAlongCurve(targetObject.position, startPoint, endPoint, height);
        
//        //frameToPlay = distanceAlongCurve;
//        //Debug.Log("Distance along curve: " + distanceAlongCurve);

//        // Calculate the positions of the points on the arc

//        //Vector3[] positions = new Vector3[resolution + 1];
//        //for (int i = 0; i <= resolution; i++)
//        //{
//        //    float t = (float)i / resolution;
//        //    Vector3 point = Vector3.Lerp(startPoint, endPoint, t);
//        //    point.y += height * Mathf.Sin(t * Mathf.PI);
//        //    positions[i] = point;
//        //}

//        // Set the positions of the line renderer
//        //lineRenderer.SetPositions(positions);


//    }


//    private void OnTriggerEnter(Collider other)
//    {
//        if (other.gameObject.CompareTag("Trail"))
//        {
//            inTrail = true;
//        }

//        if (other.gameObject.CompareTag("EndPoint"))
//        {

//            EventManager.TriggerEvent(Events.FoldingSuccess);
//            archObject.GetComponent<MeshRenderer>().enabled = false;
//            nodeObject.SetActive(false);

//        }

//    }

//    private Vector3 CalculateBezierPoint(float t, Vector3[] points)
//    {
//        int numPoints = points.Length;

//        for (int i = 1; i < numPoints; i++)
//        {
//            for (int j = 0; j < numPoints - i; j++)
//            {
//                points[j] = (1 - t) * points[j] + t * points[j + 1];
//            }
//        }

//        return points[0];
//    }


//    private void OnTriggerExit(Collider other)
//    {
//        if (other.gameObject.CompareTag("Trail") && failState == true)
//        {
//            inTrail = false;
//            //nodeRigidBody.transform.position = startPosition;
//            nodeRigidBody.velocity = Vector3.zero;
//            EventManager.TriggerEvent(Events.FoldingFail);
//        }
//    }

//    //private float CalculateCurveLength(Vector3 start, Vector3 end, float height)
//    //{
//    //    float a = Vector3.Distance(start, end) / 2f;
//    //    float b = height;
//    //    float c = Mathf.Sqrt(Mathf.Pow(a, 2) + Mathf.Pow(b, 2));
//    //    return 2 * Mathf.PI * c;
//    //}

//    //private float CalculateDistanceAlongCurve(Vector3 position, Vector3 start, Vector3 end, float height)
//    //{
//    //    float a = Vector3.Distance(start, end) / 2f;
//    //    float b = height;
//    //    float c = Mathf.Sqrt(Mathf.Pow(a, 2) + Mathf.Pow(b, 2));

//    //    Vector3 dir = (end - start).normalized;
//    //    Vector3 perp = Vector3.Cross(dir, Vector3.up).normalized;
//    //    float t = Vector3.Dot(position - start, dir);
//    //    float y = Vector3.Dot(position - start, perp);
//    //    float x = Mathf.Sqrt(Mathf.Pow(c, 2) - Mathf.Pow(y - b, 2));

//    //    float distanceAlongCurve = 2 * c * Mathf.Asin(x / (2 * c));
//    //    if (t < 0)
//    //    {
//    //        distanceAlongCurve = -distanceAlongCurve;
//    //    }
//    //    else if (t > Vector3.Distance(start, end))
//    //    {
//    //        distanceAlongCurve += Vector3.Distance(start, end);
//    //    }

//    //    return distanceAlongCurve;
//    //}

//}

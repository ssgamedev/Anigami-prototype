using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

    public Vector3 cameraSwitch;
    public Quaternion cameraSwitchAngle;

    public Vector3 oldPosition;
    public Quaternion oldRotation;

    void Start()
    {
        EventManager.StartListening(Events.InitiateFolding, Initiate);

        oldPosition = GameObject.Find("Main Camera").transform.position;
        oldRotation = GameObject.Find("Main Camera").transform.rotation;

    }

    private void Update()
    {
        cameraSwitch = GameObject.Find("CameraSwitch").transform.position;
        cameraSwitchAngle = GameObject.Find("CameraSwitch").transform.rotation;
    }
    void Initiate()
    {
        transform.position = cameraSwitch;
        transform.rotation = cameraSwitchAngle;

        EventManager.StopListening(Events.InitiateFolding, Initiate);
        EventManager.StartListening(Events.EndFolding, End);

    }

    void End()
    {
        transform.position = oldPosition;
        transform.rotation = oldRotation;

        EventManager.StartListening(Events.InitiateFolding, Initiate);
        EventManager.StopListening(Events.EndFolding, End);

    }
}

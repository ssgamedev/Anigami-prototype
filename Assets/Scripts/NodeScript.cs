using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeScript : MonoBehaviour
{

    public Rigidbody nodeRigidBody;
    public GameObject pointPrefab;
    GameObject nodeObject;
    GameObject endPointObject;

    Transform startPoint;
    Transform endPoint;
    public Quaternion startRotation;

    public int numObjects = 10;
    float height = 1f;
    float length;
    public static float frameToPlay;
    public float pointIndex;

    //public GameObject[] pointList;
    public List<GameObject> pointList;

    private void OnEnable()
    {

        nodeObject = GameObject.Find("Node");
        nodeRigidBody = GetComponent<Rigidbody>();
        endPointObject = GameObject.Find("End Point");

        endPoint = endPointObject.transform;
        startPoint = nodeObject.transform;

        startRotation = nodeObject.transform.rotation;

        length = Vector3.Distance(endPoint.transform.position, startPoint.transform.position);

        height = length / 2;

        Vector3[] positions = new Vector3[numObjects];
        for (int i = 0; i < numObjects; i++)
        {
            float t = i / (float)(numObjects - 1);
            positions[i] = Vector3.Lerp(startPoint.position, endPoint.position, t);
            positions[i].y += Mathf.Sin(t * Mathf.PI) * height;
        }

        foreach (Vector3 position in positions)
        {
            GameObject obj = Instantiate(pointPrefab, position, Quaternion.identity);
            pointList.Add(obj);
        }

        //GameObject[] points = GameObject.FindGameObjectsWithTag("Point");
        //foreach (GameObject obj in points)
        //{
        //    pointList.Add(obj);
        //}



    }

    private void Update()
    {
        transform.rotation = startRotation;
        nodeRigidBody.velocity = Vector3.zero;
        frameToPlay = pointIndex/numObjects;
    }


    void OnTriggerEnter(Collider other)
    {

        for (int i = 0; i < pointList.Count; i++)
        {
            if (other.gameObject == pointList[i])
            {
                pointIndex = i;
            }
        }

        if (other.gameObject.CompareTag("EndPoint"))
        {
            nodeObject.transform.position = startPoint.position;
            for (int i = 0; i < pointList.Count; i++)
            {
                Destroy(pointList[i]);
            }
            pointList.Clear();
            nodeObject.transform.position = startPoint.position;
            frameToPlay = 0;
            nodeObject.SetActive(false);
            EventManager.TriggerEvent(Events.FoldingSuccess);

        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Point"))
        {
            //Debug.Log("fail");
            nodeObject.transform.position = startPoint.position;
            EventManager.TriggerEvent(Events.FoldingFail);
        }
    }

}

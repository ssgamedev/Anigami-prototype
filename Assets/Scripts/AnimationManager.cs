using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    Animator animatorComponent;
    Animation animationComponent;
    public float frameToPlay = 0;


    void Start()
    {
        Debug.Log("frame = " + frameToPlay);
        animationComponent = GetComponent<Animation>();
        animatorComponent = GetComponent<Animator>();
        animatorComponent.speed = 0;
        //frameToPlay = 0;
    }

    

    void Update()
    {
        frameToPlay = NodeScript.frameToPlay;
        animationComponent["Scene"].time = frameToPlay;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeScriptTest : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;
    public Transform[] curvePoints;

    public int curveResolution = 20;

    public int numRays = 10;
    public float rayLength = 5f;

    private LineRenderer lineRenderer;
    private float totalDistance;
    private float currentDistance;

    public Transform nodeTransform;

    GameObject nodeObject;
    GameObject endPointObject;

    public Rigidbody nodeRigidBody;
    public Quaternion startRotation;


    private void Start()
    {
        totalDistance = Vector3.Distance(startPoint.position, endPoint.position);

        lineRenderer = GetComponent<LineRenderer>();

        for (int i = 0; i < curvePoints.Length - 1; i++)
        {
            totalDistance += Vector3.Distance(curvePoints[i].position, curvePoints[i + 1].position);
        }
    }

    private void OnEnable()
    {
        nodeObject = GameObject.Find("Node");
        nodeTransform = nodeObject.transform;
        endPointObject = GameObject.Find("End Point");
        startPoint = nodeObject.transform;
        endPoint = endPointObject.transform;
    }


    void Update()
    {
        float distanceBetweenStartAndCurrent = Vector3.Distance(startPoint.position, transform.position);
        float distanceBetweenStartAndEnd = Vector3.Distance(startPoint.position, endPoint.position);

        currentDistance = distanceBetweenStartAndEnd - distanceBetweenStartAndCurrent;

        float percentage = currentDistance / totalDistance;

        Vector3[] curvePoints = BezierCurve(startPoint.position, endPoint.position, curveResolution);
        lineRenderer.positionCount = curvePoints.Length;
        for (int i = 0; i < curvePoints.Length; i++)
        {
            lineRenderer.SetPosition(i, curvePoints[i]);
        }
    }

    private Vector3[] BezierCurve(Vector3 start, Vector3 end, int resolution)
    {
        Vector3[] points = new Vector3[resolution + 1];

        for (int i = 0; i <= resolution; i++)
        {
            float t = (float)i / resolution;
            points[i] = Vector3.Lerp(start, end, t * t * (3f - 2f * t));
        }

        return points;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetScript : MonoBehaviour
{
    public GameObject thisObject;
    public GameObject blankPaperObject;
    public void ResetButton()
    {
        blankPaperObject.SetActive(true);
        blankPaperObject.transform.position = transform.position;
        thisObject.SetActive(false);
    }
}
